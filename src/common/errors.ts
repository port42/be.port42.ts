import { Request, Response, NextFunction } from 'express';
import Logger from '../utils/logger';

export class AppError extends Error {
    constructor(
        readonly code: string,
        message: string,
    ) {
        super(message);

        // See: https://stackoverflow.com/questions/41102060/typescript-extending-error-class
        // Might have to be removed when transpiling to ES6
        Object.setPrototypeOf(this, new.target.prototype);
    }
}
export class NotFound extends AppError {
    constructor(message: string) {
        super('not.found', message);
    }
}

export class InvalidInput extends AppError {
    constructor(message: string) {
        super('bad.request', message);
    }
}

export class BadGateway extends AppError {
    constructor(message: string) {
        super('bad.gateway', message);
    }
}
export class NotAuthorized extends AppError {
    constructor(message: string) {
        super('not.authorized', message);
    }
}

export class NotAuthenticated extends AppError {
    constructor(message: string) {
        super('not.authenticated', message);
    }
}

export class Conflict extends AppError {
    constructor(message: string) {
        super('conflict', message);
    }
}

export const errorHandler = (err: any, req: Request, res: Response, next: NextFunction): void => {
    Logger.error(`Error while handling ${req.method} ${req.path}`);
    if (err.status === 401) {
        Logger.error('- auth failed: should log as an incident?', err);
        res.status(401).send({ message: 'Invalid token' });
    } else if (!(err instanceof AppError)) {
        Logger.error('- error: should return 500', err);
        next(err);
    } else {
        Logger.error('Application error:', err);

        if (err instanceof NotAuthorized) {
            Logger.error(`- not authorized`);
            Logger.error(`- reason: ${err.message}`);
            res.status(403).send({ message: err.code, details: err.message });
        } else if (err instanceof NotFound) {
            Logger.error('- resource not found');
            Logger.error(`- reason: ${err.message}`);
            res.status(404).send({ message: err.code, details: err.message });
        } else if (err instanceof Conflict) {
            Logger.error('- resource update conflict');
            Logger.error(`- reason: ${err.message}`);
            res.status(409).send({ message: err.code, details: err.message });
        } else if (err instanceof InvalidInput) {
            Logger.error('- invalid input');
            Logger.error(`- reason: ${err.message}`);
            res.status(400).send({ message: err.code, details: err.message });
        } else if (err instanceof BadGateway) {
            Logger.error('- invalid response from server');
            Logger.error(`- reason: ${err.message}`);
            res.status(502).send({ message: err.code, details: err.message });
        } else {
            Logger.error(`- unhandled error type: code = ${err.code} - returning status 500`);
            Logger.error(`- reason: ${err.message}`);
            next(err);
        }
    }
};
