export interface IBook {
    title: string;
    author: string;
}

export interface ITest {
    name: string;
    email: string;
}

export interface IQuery {
    books?: Function;
    testdb?: Function;
}

export interface IQueryResolver {
    Query: IQuery;
}
