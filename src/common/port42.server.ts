import * as express from 'express';
import { Express } from 'express';
import { Server } from 'http';
import * as listEndpoints from 'express-list-endpoints';
import Factory from '../utils/factory';
import { ModuleMapping, AppConfig, AsyncStartStop } from './types';
import Port42Service from '../services/port42.service';
import registerModules from './registrar';
import logger from '../utils/logger';
import appollo from '../graphql';

export interface WebServer extends AsyncStartStop {}

class ExpressServer implements WebServer {
    private serverInstance: Server;

    constructor(
        readonly app: Express,
        readonly port: string | number,
        readonly service: Port42Service,
    ) {}

    private showAllRoutes = () => {
        logger.debug(JSON.stringify(listEndpoints(this.app)));
    }

    private serverStartExecutor = (resolve: () => void): void => {
        this.serverInstance = this.app.listen(this.port, () => {
            this.showAllRoutes();
            logger.info('server listening');
            logger.debug(`on port ${this.port}`);
            resolve();
        });
    }

    private serverStopExecutor = (resolve: () => void): void => {
        resolve();
    }

    start = (): Promise<void> => {
        if (this.serverInstance) {
            logger.debug('cannot start server: already running');
            return Promise.resolve();
        }

        logger.debug('starting server...');
        return (
            new Promise<void>(this.serverStartExecutor)
        );
    }

    stop = (): Promise<void> => {
        if (!this.serverInstance) {
            logger.debug('cannot stop server: not running');
            return Promise.resolve();
        }
        logger.debug('stopping server...');
        return (
            new Promise<void>(this.serverStopExecutor)
        );
    }
}

export type ServerFactory = Factory<Port42Service, WebServer>;


export interface ServerFactoryParams {
    modules: ModuleMapping;
    config: AppConfig;
    port: string | number;
}

const serverFactoryFactory: Factory<ServerFactoryParams, ServerFactory> =
    ({ modules, config, port }) =>
        (service: Port42Service) => {
            const app = express();
            registerModules(app, modules, service, config);
            if (!config.server.dontUseGraphql) {
                appollo(app, config.server.graphqlPath);
            }
            return new ExpressServer(app, port, service);
        };

export default serverFactoryFactory;
