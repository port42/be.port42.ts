import { ModuleMapping, AppConfig, ModuleDef, ContextCreator, Handler } from '../common/types';
import Port42Service from '../services/port42.service';
import { Express, Router, RequestHandler, Request, Response, NextFunction } from 'express';
import logger from '../utils/logger';
import loadModule from './loader';
import { NotAuthorized } from './errors';

const wrap = <CTX> (handler: Handler<CTX>, contextCreator: ContextCreator<CTX>): RequestHandler =>
    (req: Request, res: Response, next: NextFunction) => {
        const context = contextCreator(req);

        return (
            Promise
            .resolve(
                handler.authorization(context),
            )
            .then(
                isAuthorized =>
                (!isAuthorized
                    ? Promise.resolve(new NotAuthorized('Access only for authorized users'))
                    : handler.action(context, req, res)),
            )
            .then((resultOrError) => {
                if (resultOrError instanceof Error) {
                    logger
                    // tslint:disable-next-line:ter-max-len
                    .error(`error while handling request ${req.path}: ${JSON.stringify(resultOrError)}`);
                    return next(resultOrError);
                }
                return res.send(resultOrError);
            })
            .catch(next)
        );
    };


export const setupModule = <CTX> (moduleDef: ModuleDef<CTX>,
        router: Router, contextCreator: ContextCreator<CTX>): void => {
    const registerRoute = (path: string) => {
        const endpointDef = moduleDef[path];

        if (typeof endpointDef === 'function') {
            router.get(path, wrap(endpointDef, contextCreator));
        } else {
            Object.keys(endpointDef)
            .forEach(method => router[method](path, wrap(endpointDef[method], contextCreator)));
        }
    };

    Object.keys(moduleDef)
    .forEach(registerRoute);
};

export default (
    app: Express,
    modules: ModuleMapping,
    service: Port42Service, config: AppConfig) => {
    // only load modules that are set in the configuration
    Object.keys(modules)
    .forEach((pathPrefix) => {
        const moduleName = modules[pathPrefix];

        logger.debug(`registering module ${moduleName} on '${pathPrefix}'`);
        app.use(pathPrefix, loadModule(moduleName, service, config));
    });
};
