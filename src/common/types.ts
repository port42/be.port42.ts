import { Request, Response } from 'express';
export interface AppConfig {
    server: ServerConfig;
}

export interface MailConfig {
    host: string;
    port: number;
    secure: boolean;
    username: string;
    password: string;
    templatePath: string;
}

export interface DbConfig {
    url: string;
    encrypt: boolean;
    allowCreateTables: boolean;
    dialect?: 'mysql' | 'sqlite' | 'postgres' | 'mssql';
}

export interface ServerConfig {
    env: string;
    apiUrlBase: string;
    port: number;
    corsDomain: string;
    serverName: string;
    dontUseGraphql: boolean;
    graphqlPath: string;
    httpSchema: string;
}

export interface AsyncStartStop {
    readonly start: () => Promise<void>;
    readonly stop: () => Promise<void>;
}

export type ModuleMapping = {
    [path: string]: string;
};

export interface Handler<CTX> {
    action: (context: CTX, req: Request, res: Response) => Promise<any>;
    authorization: (context: CTX) => boolean;
}

export type Methods = 'get' | 'post' | 'put' | 'delete';

export type HandlerDef<CTX> = {
    [M in Methods]?: Handler<CTX>
};

export type ModuleDef<CTX> = {
    [pathPrefix: string]: HandlerDef<CTX>;
};

export type ContextCreator<CTX> = (req: Request) => CTX;

