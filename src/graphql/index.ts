import { Express } from 'express';
import schema from './schema';

export default (app: Express, path: string) => {
    schema.applyMiddleware({
        app, path,
    });
};
