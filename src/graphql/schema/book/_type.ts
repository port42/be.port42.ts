// tslint:disable-next-line:variable-name
const Book = `
  type Book {
    title: String!
    author: String!
  }
`;

export const types = (): string[] => [Book];

export const typeResolvers = {

};
