export default class TestContext {
    private constructor() {}

    static create = () =>
        new TestContext()
}
