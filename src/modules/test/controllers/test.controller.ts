
import { Request, Response } from 'express';
import Context from '../common/test.context';
import pool from '../../../common/queries';
export default () =>
({
    info:
    // tslint:disable-next-line:ter-arrow-body-style
    // tslint:disable-next-line:variable-name
    async (_ctx: Context, req: Request, res: Response): Promise<{}> =>
        Promise
        .resolve(
            req.body,
        )
        .then(
            () => {
                res.status(200);
                return ({
                    test: 'ok',
                });
            },
    ),
    dbtest: async () => Promise.resolve().then(() => pool.query('SELECT * FROM users ORDER BY id ASC')
        .then(result => ({
            result: result.rows,
        }))),
});
