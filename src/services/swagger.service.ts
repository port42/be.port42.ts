import { AppConfig } from '../common/types';

export default class SwaggerService {
    constructor(private config: AppConfig){}

    readonly getData = () => this.config.server.serverName;
}
