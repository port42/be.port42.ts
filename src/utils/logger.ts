import { createLogger, format, transports } from 'winston';

const {
    combine, timestamp, label, printf, colorize,
} = format;

const myFormat = printf(info => `${info.level}: ${info.message}`);

const fileFormat = printf(info =>
    `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`);

const loggingConfig = {
    file: {
        level: 'error',
        filename: 'logs/beport42.log',
        handleExceptions: true,
        format: combine(
            colorize(),
            label({ label: 'be.port42' }),
          timestamp(),
          fileFormat,
        ),
        json: true,
        maxsize: 5242880,
        maxFiles: 100,
        colorize: false,
    },
    console: {
        level: 'debug',
        handleExceptions: true,
        format: combine(
      colorize(),
      label({ label: 'be.port42' }),
      timestamp(),
      myFormat,
        ),
    },
};

loggingConfig.file.filename = `${loggingConfig.file.filename}`;

export const logger = createLogger({
    transports: [
        new transports.File(loggingConfig.file),
        new transports.Console(loggingConfig.console),
    ],
    exitOnError: false,
});

// tslint:disable-next-line:variable-name
export const skip = (_req: any, res: { statusCode: number; }) => res.statusCode >= 200;

export const stream = {
    write: (message: string) => {
        logger.info(message);
    },
};

export default logger;
